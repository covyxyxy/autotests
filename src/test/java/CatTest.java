import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapper;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class CatTest {

    @Test
    public void addCatTest() {
        given().
                when().
                contentType(ContentType.JSON)
                .log().all()
                .body("""
                        {
                        "id": 10008,
                        "name": "Буся",
                        "status": "available"
                        }
                        """)
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .log().all().
                statusCode(200);



        given().
                when().
                contentType(ContentType.JSON)
                .log().all()
                .body("""
                        {
                        "id": 10008,
                        "name": "Муся",
                        "status": "available"
                        }
                        """)
                .put("https://petstore.swagger.io/v2/pet")
                .then()
                .log().all()
                .statusCode(200);



        given().
                when().
                contentType(ContentType.JSON)
                .log().all()

                .get("https://petstore.swagger.io/v2/pet/10008")
                .then()
                .log().all()
                .statusCode(200)
                .body("name", is("Муся"));


        given().
                when().
                contentType(ContentType.JSON)
                .log().all()
                .delete("https://petstore.swagger.io/v2/pet/10008")
                .then()
                .log().all()
                .statusCode(200);

        given().
                when().
                contentType(ContentType.JSON)
                .log().all()

                .get("https://petstore.swagger.io/v2/pet/10008")
                .then()
                .log().all()
                .statusCode(404);


    }


}
