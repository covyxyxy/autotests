import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

public class Test3 {
    private final List<String> testNames = List.of(
            "Busya",
            "BUSYA",
            "busya",
            "BuSyA",
            "Буся",
            "БУСЯ",
            "буся",
            "БуСя",
            "0887653"
    );

    @Test
    public void petNames() {
        for (int i = 0; i < testNames.size(); i++) {
            String name = testNames.get(i);

            given().
                    when().
                    contentType(ContentType.JSON)
                    .log().all()
                    .body("""
                            {
                                "id": 10008%d,
                            "name": "%s",
                            "status": "available"
                            }
                            """.formatted(i, name))
                    .post("https://petstore.swagger.io/v2/pet")
                    .then()
                    .log().all().
                    statusCode(200);
        }


        for (int i = 0; i < testNames.size(); i++) {
            String name = testNames.get(i);
            given().
                    when().
                    contentType(ContentType.JSON)
                    .log().all()

                    .get("https://petstore.swagger.io/v2/pet/10008" + i)
                    .then()
                    .log().all()
                    .statusCode(200)
                    .body("name", is(name));

        }

    }

    @Test
    public void orderTest() {
        Response response = given().
                when().
                contentType(ContentType.JSON)
                .log().all()
                .body("""
                        {
                          "id": 0,
                          "petId": 0,
                          "quantity": 0,
                          "shipDate": "2022-08-01T15:00:56.891Z",
                          "status": "placed",
                          "complete": true
                        }
                            """)
                .post("https://petstore.swagger.io/v2/store/order");

        JsonPath jsonPathEvaluator = response.jsonPath();


        int petId = jsonPathEvaluator.getInt("petId");
        String status = jsonPathEvaluator.getString("status");
        try {
            jsonPathEvaluator.getBoolean("complete");

        } catch (Exception e) {
            fail("это не булеан");
        }

        String shipDate = jsonPathEvaluator.getString("shipDate");

        assertNotNull(shipDate);
        assertEquals(0, petId);
        assertTrue(List.of("placed", "approved", "delivered").contains(status));


    }
    @Test
    public void testJsonObject(){
        JSONObject bodyJO = new JSONObject()
                .put("id", 5)
                .put("name","Busya")
                .put("status", "available");

        given().
                when().
                contentType(ContentType.JSON)
                .log().all()
                .body(bodyJO.toString())
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .log().all().
                statusCode(200);
    }
}
